<?php get_header(); ?>

		<div id="main"><div class="content">
	<?php if (have_posts()) : ?>
	<?php $postnumber = '1' ?>

 	  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
 	  <?php /* If this is a category archive */ if (is_single()) { ?>
		<h2 class="pagetitle"><a target="_blank" href="http://feeds.feedburner.com/perevodman" rel="nofollow"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/rss.png" alt="RSS подписка" title="RSS подписка" width="30" height="30" /></a> <a target="_blank" href="https://twitter.com/PEREVODMAN" rel="nofollow"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/twitter.png" alt="Follow Me" title="Follow Me" width="30" height="30" /></a> <a target="_blank" href="http://www.facebook.com/perevodman" rel="nofollow"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/facebook.png" alt="Переводман на Facebook" title="Переводман на Facebook" width="30" height="30" /></a> <a target="_blank" href="http://vk.com/perevodomania" rel="nofollow"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/vkontakte.png" alt="Переводман Вконтакте" title="Переводман Вконтакте" width="30" height="30" /></a> <a href="http://perevodman.com/about/email/"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/mail.png" alt="Подписка на почтовую рассылку" title="Подписка на почтовую рассылку" width="30" height="30" /></a></h2>
 	  <?php } ?>
 	 

		<?php while (have_posts()) : the_post(); ?>
			<div class="post" id="post-<?php the_ID(); ?>">
				<div class="title">
					<h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					<div class="meta"><span style="color: #ff0000;"><?php the_author() ?></span>&nbsp;&nbsp;&nbsp;<?php edit_post_link('Редактировать', ' | ', ' | '); ?></div>
					<div class="date"><?php the_time('j F') ?>,<br /><?php the_time('Y') ?></div>
				</div>
				<div class="entry">
					<?php the_content(''); ?>

					<?php wp_link_pages(array('before' => '<p><strong>Страницы:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

				</div>
				<div class="postmetadata"><div align="left" style="float:left"><script type="text/javascript"><!--
document.write(VK.Share.button(false,{type: "button_nocount", text: "Сохранить"}));
--></script></div>
<?php the_tags( 'Связанные: ', ', ', ''); ?>&nbsp;<span class="comm-star"> Тема: <?php the_category(', ') ?></span>
</div>
				
			</div>

<?php comments_template(); ?>

	    <?php endwhile; ?>
		<?php else : ?>
			<div class="post">
				<h2 class="center">Not Found</h2>
				<p class="center">Sorry, but you are looking for something that isn't here.</p>
			</div>
	<?php endif; ?>
		</div></div>	
<?php get_sidebar(); ?>

<?php get_footer(); ?>