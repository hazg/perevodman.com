		<div class="comments">
			<div id="comments">
			
			<?php 
			global $sape;
			echo $sape->return_links(1);
			?>
<?php if ( function_exists('wp_list_comments') ) : ?>

<?php if ( have_comments() ) : ?>

            <?php
            echo "<h4>Комментарии</h4>";
            /*
			<h3 id="comments-title"><?php
			printf( _n( 'One Response to %2$s', '%1$s Responses to %2$s', get_comments_number(), 'twentyten' ),
			number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' );
			?></h3>
            */
            ?>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> старые', 'twentyten' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'новые <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
			</div> <!-- .navigation -->
<?php endif; // check for comment navigation ?>

				<?php
					/* Loop through and list the comments. Tell wp_list_comments()
					 * to use twentyten_comment() to format the comments.
					 * If you want to overload this in a child theme then you can
					 * define twentyten_comment() and that will be used instead.
					 * See twentyten_comment() in twentyten/functions.php for more.
					 */
					wp_list_comments( array( 'callback' => 'jeans3c_comment' ) );
				?>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> старые', 'twentyten' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'новые <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
			</div><!-- .navigation -->
<?php endif; // check for comment navigation ?>

<?php else : // or, if we don't have comments:

	/* If there are no comments and comments are closed,
	 * let's leave a little note, shall we?
	 */
	if ( ! comments_open() ) :
?>
	<p class="nocomments"><?php _e( 'Comments are closed.', 'twentyten' ); ?></p>
<?php endif; // end ! comments_open() ?>

<?php endif; // end have_comments() ?>
<?php else: ?>
<!-- Start CommentsList-->
<?php // Do not delete these lines
if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
die ('Please do not load this page directly. Thanks!');
if (!empty($post->post_password)) { // if there's a password
if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
?>
<p class="nocomments">This post is password protected. Enter the password to view comments.<p>
<?php return; }}
		/* This variable is for alternating comment background */
		$oddcomment = 'alt ';
?>
<!-- You can start editing here. -->
		<?php if ($comments) : ?>
			<?php /* Count the totals */
				$numPingBacks = 0;
				$numComments  = 0;
				foreach ($comments as $comment) {
					if (get_comment_type() != "comment") {
						$numPingBacks++;
					} else {
						$numComments++;
					}
				}
			?>
			<?php if ($numComments != 0) : ?>
					<h4><?php if ($numComments == 1) { echo 'Комментарии'; } else { echo 'Комментарии'; } ?></h4>
				<?php $i = 0; ?>
				<?php if (get_option('comment_order') == 'asc') rsort($comments); ?>
				<?php foreach ($comments as $comment) : ?>
				<?php $i++; ?>
				<?php if (get_comment_type() == 'comment'){ ?>
					<div class="<?php if ($comment->comment_author_email == get_the_author_email()) echo 'author_'; else echo $oddcomment; ?>message-box" id="comment-<?php comment_ID() ?>">
						<div class="message-lbc"></div><div class="message-ltc"></div>
						<div class="message-entry">
							<div class="message-by"><?php comment_author_link() ?></div>
							<div class="message-time"><span><a href="#comment-<?php comment_ID() ?>"><?php comment_date('j F Y') ?> <?php comment_time() ?></a></span>&nbsp;&nbsp;&nbsp;<?php echo "&#35;$i" ?>&nbsp;&nbsp;&nbsp;<?php edit_comment_link('<strong>Редактировать коммент</strong>',' ',' '); ?></div>


							<!-- <div class="comment-num"><? /* php gravatar() */ ?><?php $commentNumber++; echo $commentNumber; ?></div> -->

							<?php if ($comment->comment_approved == '0') : ?><em>Все комментарии, содержащие ссылки и фамилии метров автоматически попадают на премодерацию...</em><?php endif; ?>
							<?php comment_text() ?>
						</div>
					</div>
					<?php /* Changes every other comment to a different class */
						if ('alt ' == $oddcomment) $oddcomment = '';
						else $oddcomment = 'alt ';
					?>
				<?php } /* End of is_comment statement */ ?>
				<?php endforeach; /* end for each comment */ ?>
			<?php endif; ?>
		
			<?php if ($numPingBacks != 0) : ?>
					<h4 id="trackbacks"><?php if ($numPingBacks == 1) { echo 'One Trackback/Ping'; } else { echo $numPingBacks; echo ' Trackbacks/Pings'; } ?></h4>
					<ol class="commentlist">
				<?php foreach ($comments as $comment) : ?>
					<?php if (get_comment_type() != 'comment'){ ?>
						<li class="<?php echo $oddcomment; ?>ping" id="comment-<?php comment_ID() ?>">
							<cite>
								<span class="author b"><?php comment_author_link() ?>&nbsp;&nbsp;&nbsp;</span>
								<span class="date"><?php comment_date('M d Y') ?> / <?php comment_date('ga') ?>:</span>
							</cite>
							<div class="tb-text">
								<?php if ($comment->comment_approved == '0') : ?>
									<em>Все комментарии, содержащие ссылки и фамилии метров автоматически попадают на премодерацию...</em>
								<?php endif; ?>
								<?php comment_text() ?>
							</div>
						</li>
						<?php /* Changes every other comment to a different class */
						if ('alt' == $oddcomment) $oddcomment = '';
						else $oddcomment = 'alt';
						?>
					<?php } ?>
				<?php endforeach; /* end for each comment */ ?>
					</ol>
			<?php endif; ?>


		<?php else : /* this is displayed if there are no comments so far */ ?>
			<?php if ('open' == $post->comment_status) : ?> 
			<!-- If comments are open, but there are no comments. -->
			<?php else : /* comments are closed */ ?>
			<!-- If comments are closed. -->

			<?php endif; ?>
		<?php endif; ?>
<?php endif; // if ( function_exists('wp_list_comments') ) ?>
		<?php if ('open' == $post->comment_status) : ?>
		<!-- Ends CommentsList-->
			</div>

<!-- Start Comments Form-->
			<div id="respond">
				<?php if ( $user_ID ) : ?>
					<h4>Комментарий появится в переулке после проверки модератором</h4>
				<?php else : ?>
					<h4><a href="http://perevodman.com/wp-login.php">Вход</a> | <a href="http://perevodman.com/wp-register.php">Регистрация</a></h4>
				<?php endif; ?>
					<div class="form">
<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<?php else : ?>
					<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
						<div class="inputs">
	<?php if ( $user_ID ) : ?>
	<p>Это ты: <a href="<?php echo get_option('siteurl'); ?>/author/<?php echo $user_identity; ?>"><?php echo $user_identity; ?></a> | <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Выйти</a></p>
	<?php else : ?>
							Name <?php if ($req) _e('(<b>*</b>)'); ?>
							<div class="input"><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" tabindex="1" /></div>
							Mail <?php if ($req) _e('(<b>*</b>)'); ?>
							<div class="input"><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" tabindex="2" /></div>
							URI
							<div class="input"><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" tabindex="3" /></div>
	<?php endif; ?>
						</div>
						<div class="message">
							Комментарий (Ctrl+Enter):
							<div class="input"><textarea name="comment" id="comment" rows="7" cols="25" tabindex="4"></textarea></div>

<script type="text/javascript">
function addsmile($smile){
  $('#comment').replaceSelectedText(' ' + $smile + ' ');
}
</script>
<?php
global $wpsmiliestrans;
$dm_showsmiles = '';
$dm_smiled = array();
if ($wpsmiliestrans) {
foreach ($wpsmiliestrans as $tag => $dm_smile) {
    if (!in_array($dm_smile,$dm_smiled)) {
        $dm_smiled[] = $dm_smile;
        $tag = str_replace(' ', '', $tag);
        $dm_showsmiles .= '<img src="'.get_bloginfo('wpurl').'/wp-includes/images/smilies/'.$dm_smile.'" alt="'.$tag.'" onclick="addsmile(\''.$tag.'\');"/> ';
    }
}
echo '<div style="width:225px; margin-left:0px; margin-right:0px;">'.$dm_showsmiles.'</div>';
}
?>

						</div>
						<div class="clear"></div>
						<div class="submit"><input type="submit" value=" " title="Post Your Comment" /><input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" /></div>
						<?php do_action('comment_form', $post->ID); ?>
					</form>
<?php endif; /* If registration required and not logged in */ ?>
				</div>	
			</div>
<?php endif; /* if you delete this the sky will fall on your head */ ?>
		</div>
