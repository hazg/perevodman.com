<?php get_header(); ?>

		<div id="main"><div class="content">
			<h2 class="pagetitle">Ищем: <?php echo $s; ?></h2>
	<?php if (have_posts()) : ?>
	<?php $postnumber = '1' ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="post" id="post-<?php the_ID(); ?>">
				<div class="title">
					<h1><?php the_category(' '); ?> » <a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					<div class="meta"><span style="color: #ff0000;"><?php the_author() ?></span>&nbsp;&nbsp;&nbsp;<?php edit_post_link('Редактировать', ' | ', ' | '); ?></div>
					<div class="date"><?php the_time('j F') ?>,<br /><?php the_time('Y') ?></div>
				</div>
				<div class="entry">
					<?php if (function_exists('the_excerpt_reloaded')) 
						the_excerpt_reloaded(145, '<a><img>', 'excerpt', TRUE, '');
					else
						the_content(__(''));
					?>
					<?php wp_link_pages(); ?>

					<?php if($postnumber < '3') { ?>
						<div class="google">
					<!-- Put Your Google Ads Here -->
						</div>
					<?php } $postnumber++; ?>
				</div>
				<div class="postmetadata"><?php the_tags( 'Связанные: ', ', ', ''); ?>&nbsp;<span class="comm-star"> Тема: <?php the_category(', ') ?>&nbsp;<span class="comm-star"><?php comments_popup_link('', 'Комменты: %', 'Комменты: %'); ?></span></div>
			</div>
	    <?php endwhile; ?>
		<?php else : ?>
			<div class="post">
				<h2 class="center">Совпадений нет. Зато есть Мариса Томей! Это куда интереснее...</h2>
<a href="http://perevodman.com/podborki/3493/"><img src="http://perevodman.com/gallery/img/MarisaTomei.gif" alt="" title="Ничего не найдено" width="244" height="754" class="aligncenter size-full wp-image-4316" /></a>
				<div class="google">
					<!-- Put Your Google Ads Here -->
				</div>
			</div>
	<?php endif; ?>
			<?php if(function_exists('wp_pagenavi')) { ?>
				<div class="pagenavi-bg"><div class="pagenavi-r"><div class="pagenavi-l"><?php wp_pagenavi(); ?></div></div></div>
			<?php } else { ?> 
				<p class="center"><?php previous_posts_link('&laquo; следующие новости') ?>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<?php next_posts_link('предыдущие новости &raquo;') ?></p>
			 <?php } ?>
			<!-- <p class="center"><?php posts_nav_link(' - ','&#171; Newer Posts','Older Posts &#187;') ?></p> -->
		</div></div>	
<?php get_sidebar(); ?>

<?php get_footer(); ?>