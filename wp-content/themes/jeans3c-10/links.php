<?php
/*
Template Name: Links
*/
?>

<?php get_header(); ?>

		<div id="main"><div class="content">
			<div class="post">
				<div class="entry">
					<h2>Links:</h2>
					<ul>
					<?php get_links_list(); ?>
					</ul>
				</div>
			</div>
		</div></div>	
<?php get_sidebar(); ?>

<?php get_footer(); ?>