/*
 * Authors only
 * */

jQuery(function($){

  /*
   * SEO
   */
  var mediafiles = $('#TB_iframeContent').contents();

  // Replace keywords input with textarea
  $('[name="psp_keywords"]').each(function(){
    $(this).replaceWith('<textarea name="psp_keywords" rows="4" cols="60">'+$(this).val()+'</textarea>');
  });


  
  /* Editor */

  $('#publish').click(function(e){
    $('.perevodman-required').remove();
    ret = true;
    
    var check_element = function(el, string){
      var this_clone = $(el).clone();
      this_clone.find('*').remove();
      var search_string = this_clone.text().trim();
      return string.match(new RegExp(search_string, 'i'));
    };
    
    if ( $('.wp-editor-area').val().indexOf('<!--more-->') == -1 ){
      ret = false;
      $('#wp-content-editor-container').prepend('<div style="float:right;margin-top: -20px; right: 0px;position: relative;" class="perevodman-required">Используйте "Превью для главной"</div>');
    }
    
    $('#psp_description, [name=psp_keywords]').each(function(){
      var input = $(this);
      var val = input.val();

      if (val.length < 1) {
        $(this).focus().parent().prepend($('<div class="perevodman-required" style="">Обязательно для заполнения</div>'));
        ret = false;
      }
      if(ret && input.attr('id') != 'psp_description'){
        // Check tag names for duplicate
        $('.tagchecklist span').each(function(){
          if(check_element(this, val)){
            input.focus().parent().prepend($('<div class="perevodman-required" style="">Обязательно для заполнения (не дублировать метки и рубрики)</div>'));
            ret = false;
          }
        });

        // Check category names for duplicate
        $('.categorychecklist > li label').each(function(){
          if(check_element(this, val)){
            input.focus().parent().prepend($('<div class="perevodman-required" style="">Обязательно для заполнения (не дублировать метки и рубрики)</div>'));
            ret = false;
          }
        });
      }
    }).bind('click, keydown', function(){
      var label = $(this).prev();
      if(label.is('div.perevodman-required')){
        label.remove();
      }
    });
    if (!ret) {
      e.preventDefault();
      e.stopPropagation();
    }
    $(e.currentTarget).removeClass('button-primary-disabled');
    $('#ajax-loading').css({'visibility':'hidden'});
    
    return ret;
  });
});
