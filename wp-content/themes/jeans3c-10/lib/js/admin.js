jQuery(function($){

  



  /*
   * Help link
   */
  $('#contextual-help-link').unbind().click(function(){
    window.location = '/articles/1198/';
  });


  /*
   * Editor height
   */
  var categorydiv = $('#categorydiv');
  var editor = $('.wp-editor-area');
  if ( categorydiv.size() > 0 ) {
    editor.height(
      (categorydiv.height() + categorydiv.offset().top) - editor.offset().top
    );  
  }
  
  /*
   * Mediafiles
   */
  
  // Submit
  $(document).on('click', '.media-upload-form input[type=submit]', function(){
    var ret = true;

    var container = $(this).parents('table').first(); // Current container
    if ( container.size() < 0 ) {
      container = $(this).parents('form').first();
    }
    var dims      = container.find('.media-item-info [id^=media-dims]') // width height
                    .text().split(/\s×\s/);
    var txts      = container.find('.image_alt input, .post_title input'); // text inputs

    // Height validation
    if (parseInt(dims[1]) > 650 && !perevodman.user.is_admin){
      alert('Высота не более 650 px');
      ret = false;
    }
    
    // Texts validation
    $('.perevodman-required').remove();
    txts.each(function() {
      var val = $(this).val();
      if (val.length < 1 || val.indexOf('kinopoisk.ru') !== -1 ) {
        $(this).parent().prepend($('<div class="perevodman-required" style="position:absolute;color:red;margin-top:-12px;">Обязательно для заполнения</div>'));
        ret = false;
      }
    });

    return ret;
  });

});
