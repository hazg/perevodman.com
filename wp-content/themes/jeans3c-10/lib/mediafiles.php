<?php  
  
  /*
   * Turn off flash uploader
   */
  function disable_flash_uploader() {
      return $flash = false;
  }
  add_filter( 'flash_uploader', 'disable_flash_uploader', 7 );

  /*
   * Blank default titles
   */
  add_filter('attachment_fields_to_edit','set_default_imgtitle', 10, 3);
  function set_default_imgtitle($fields, $post) {
    if ( $_REQUEST['fetch'] == 2 or $_REQUEST['fetch'] == 1 )
      $fields['post_title']['value'] = '';
    return $fields;
  }
  
  /*
    [img]
    [img=left]
    [img=right]
    [img=center] (in TZ: [align=center]
  */
  function bbcode_insert_image($html, $id, $caption, $title, $align, $url) {
    
    $post_url = get_permalink(get_post_field( 'post_parent', $id ));
    $caption = get_post_meta( $id, '_wp_attachment_image_alt', true );
    
    $caption = preg_replace('/\[|\]/', '', $caption);
    $title = preg_replace('/\[|\]/', '', $title);

    $attachment = get_post($id);
    $img = wp_get_attachment_image_src($id, 'full');
    if ($align == 'center') {
      $bbcode = "[align=center][url title=\"$caption\" self=1][img alt=\"$title\"]".$img[0]."[/img][/url][/align]";
    }  elseif($align == 'none'){
      $bbcode = "[url title=\"$caption\" self=1][img alt=\"$title\"]$img[0]"."[/img][/url]";
    }else{
      $bbcode = "[url title=\"$caption\" self=1][img=$align alt=\"$title\"]".$img[0]."[/img][/url]";
    }

    return $bbcode;
  }
  add_filter( 'image_send_to_editor', 'bbcode_insert_image', 10, 9 );

  
?>
