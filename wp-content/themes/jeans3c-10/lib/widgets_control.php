<?php

if( !is_user_logged_in() ) 
  add_filter('sidebars_widgets', 'remove_widgets_for_unregistered', 3001);

function remove_widgets_for_unregistered($all_widgets)
{
  $to_remove = array(
    'useronline',
    'shoutbox'
    
  );


  foreach( $all_widgets as $wk => $widget ) {
    foreach( $widget as $i => $inst ) {
      if( strposa($inst, $to_remove) !== false ) {
        unset( $all_widgets[$wk][$i] );
      }
    }
  }
  return $all_widgets;
}

?>
