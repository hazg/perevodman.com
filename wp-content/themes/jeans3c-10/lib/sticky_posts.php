<?php

/*
 * JS Variables
 */
add_action( 'admin_head', 'sticky_status' );
function sticky_status(){
  global $pagenow, $wpdb;
  if (is_admin() && $pagenow=='post.php'){ ?>
    <script type="text/javascript">
      jQuery(function($){
        $('#visibility-radio-password').hide();
        var br = $('[for=visibility-radio-password]').hide().next();
        br.after('<span id="sticky-span"><input id="pm-sticky" name="pm-sticky" type="checkbox" value="sticky" tabindex="4"> <label for="pm-sticky" class="selectit">Прикрепить как автор</label><br></span>')
          .remove();
        if(br.is('br')){ br.remove(); }

        $('#pm-sticky').attr('checked', <?php print (get_post_meta(get_the_ID(), 'pmsticky')?'true':'false'); ?>);
      });
    </script>
  <?php }
}


/*
 * Add / remove meta
 */
function make_pw_sticky( $post_id ) {
  remove_action( 'save_post', 'make_pw_sticky', 13, 2 );	
  print 'test';  
  #TODO: WTF?
	#if ( wp_is_post_revision( $post_id ) )
  #return;

  
  if(array_key_exists('pm-sticky', $_POST) and $_POST['pm-sticky'] == 'sticky'){
    //print_r($_POST);
    global $wpdb;
    $sticky_posts_count = $wpdb->get_var("SELECT COUNT(meta_key) FROM $wpdb->postmeta WHERE meta_key = \"pmsticky\"" );
    if($sticky_posts_count < 11) update_post_meta( $post_id, 'pmsticky', 'yes'  ); 
  
  }else{
    delete_post_meta($post_id, 'pmsticky');
  }

}
add_action( 'save_post', 'make_pw_sticky', 99, 2 );




?>
