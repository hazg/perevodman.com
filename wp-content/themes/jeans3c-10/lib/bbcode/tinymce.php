<?php
  $bbcode_buttons = array(
    'bold'
  );
    
  add_filter('tiny_mce_before_init', 'bbcode_tinymce' );

  function bbcode_tinymce($c) {
    $c['plugins']='inlinepopups,tabfocus,paste,media,fullscreen,wordpress,wpeditimage,wpgallery,wplink,wpdialogs,wpfullscreen';
    return $c;    
  }


  // init process for registering our button
  add_action('init', 'bbcode_buttons_init');
  function bbcode_buttons_init() {

    //Abort early if the user will never see TinyMCE
    if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
    return;

    //Add a callback to regiser our tinymce plugin   
    add_filter("mce_external_plugins", "bbcode_register_tinymce_plugin"); 

    // Add a callback to add our button to the TinyMCE toolbar
    add_filter('mce_buttons', 'bbcode_add_tinymce_buttons');
  }


  //This callback registers our plug-in
  function bbcode_register_tinymce_plugin($plugin_array) {

    $plugin_array['bbcode_buttons'] = get_template_directory_uri().'/lib/bbcode/js/tinymce.js';
    return $plugin_array;
  }

  //This callback adds our button to the toolbar
  function bbcode_add_tinymce_buttons($buttons) {
    //Add the button ID to the $button array
    $buttons[] = "bb_bold_button";
    return $buttons;
  }
  #
  # Configuration
  #
  /* add_filter('tiny_mce_before_init', 'bbcode_tinymce' );

  function bbcode_tinymce($c) {
    //$c['general.debug'] = true;
    $c['plugins']='inlinepopups,tabfocus,paste,media,fullscreen,wordpress,wpeditimage,wpgallery,wplink,wpdialogs,wpfullscreen';
    return $c;    
  }

  function bbcode_buttonhooks() {
    add_filter("mce_external_plugins", "bbcode_register_tinymce_javascript");
    add_filter('mce_buttons', 'bbcode_register_buttons');
  }

  function bbcode_register_buttons($buttons) {
    array_push($buttons, "button_eek", "BbcodeButtons");
    return $buttons;
  }

  // Load the TinyMCE plugin
  function bbcode_register_tinymce_javascript($plugin_array) {
    $plugin_array['bbcode'] = get_template_directory_uri().'/lib/bbcode/js/tinymce-bbcode.js';
    return $plugin_array;
  }

  // init process for button control
  add_action('init', 'bbcode_buttonhooks');*/

?>
