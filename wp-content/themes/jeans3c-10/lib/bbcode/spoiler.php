<?php
  function insert_spoiler_css()
  {
	  echo "<style type='text/css'>
	    .spoiler { border: 1px dashed; }
	    .spoiler legend { padding-right: 5px; color: #CA7A1D;}
	    .spoiler legend input { background-color: #412C14; width: 30px; }
	    .spoiler div { margin: 5px; overflow: hidden; height: 0; }
	  </style>\n";
  }
  add_action('wp_head', 'insert_spoiler_css');

  function insert_spoiler_js()
  {
echo <<<ENDJS
	  <script type='text/javascript'>
	    function tiny_spoiler( id )
	    {
		    if ( document.getElementById( id ).style.height == 'auto' )
		    {
			    document.getElementById( id ).style.height = 0;
			    document.getElementById( id ).style.padding = 0;
			    document.getElementById( id + '_button' ).value = '+';
		    }
		    else
		    {
			    document.getElementById( id ).style.height = 'auto';
			    document.getElementById( id ).style.padding = '10px';
			    document.getElementById( id + '_button' ).value = '-';
		    }
	    }
	  </script>
ENDJS;
  }
  add_action('wp_footer', 'insert_spoiler_js');

  function replace_spoiler_tag( $name, $content )
  {
	  $caracteres = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	  $addition = '';
	  for ( $i = 0; $i < 10; $i++ )
		$addition .= $caracteres[rand(0,25)];
	  $id = str_replace(' ', '', $name).$addition;
	  $s = '<fieldset class="spoiler">
			<legend>
				<input type="button" onclick="tiny_spoiler(\''.$id.'\')" id="'.$id.'_button" value="+" />
				'.$name.'
			</legend>
			<div id="'.$id.'">'
				.do_shortcode($content).'
			</div>
		</fieldset>';		
	  return $s;
  }
?>
