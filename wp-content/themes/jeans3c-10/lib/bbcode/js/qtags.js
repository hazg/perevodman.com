jQuery(function($){
    
    /*
     * Perevod bbtag
     *
     * Example: [rutracker=Имя переводчика]
     */
    


    /*
     * Toggle rutracker button
     */
    $(document).ready(function($){

      var wp_editor_area = $('.wp-editor-area');

      var toggle_rutracker_button = function (){
        var rutracker_button = rutracker_button?rutracker_button:$('#qt_content_rutracker');
        if (wp_editor_area.val().match(/\[rutracker=.*?\]/) ){
          rutracker_button.show();
        }else{
          rutracker_button.hide();
        };
      };

      wp_editor_area.on('keyup click', function (){
        toggle_rutracker_button();
      });

      $('#qt_content_rutracker').waitUntilExists(function(){ toggle_rutracker_button();});

    });
    
    /*
     * Textarea to rutracker
     */
    $.fn.to_rutracker = function (handler, shouldRunHandlerOnce, isChild) {
      
      var text = $this = $(this.selector).val();
      var rutracker = text.match(/\[rutracker=(.*)?\]([^]*)\[\/rutracker\]/);
      if ( rutracker != null ){
        
      }else{  throw "Missing [rutracker] tag!"; }


      var rx = /\[*?\]\[url=("|)http:\/\/perevodman\.com\/.*?\/(.*?)\/.*?\/*\](.*?)\/(.*?)\[\/url\]/g; // English only
      //var rx = /\[*?\]\[url=("|)http:\/\/perevodman\.com\/.*?\/(.*?)\/.*?\/*\](.*)(|\/.*?)\[\/url\]/g;
      
      var replace_with = function (text, m1, m2, m3){

        var q = (rutracker[1] + ' ' + m3).replace(/`|"|'/, ' '); // For rutracker parser


        return '][url=http://rutracker.org/forum/tracker.php?f='+window.rutracker_catalogs+'&nm='+encodeURIComponent(cutString(q, 60))+']'+m3+'[/url]';
      }
      
      return rutracker[2].replace(rx, replace_with);

    }
    
    /*
     * Rutracker button click
     */
    var rutracker_button_click = function (){
      $('.to-rutracker-dialog').remove();
      var text = $('.wp-editor-area').to_rutracker();
      $('<div>')
        .addClass('to-rutracker-dialog')
        .css({
          'overflow': 'hidden'
        })
        .append( $('<textarea>').addClass('to-rutracker-text')
          .css({
            'height': '100%',
            'width': '100%'
          })
          .text(text)
          .setSelection(0, -1)
          )
        .dialog(
          {
            'title': 'Код для рутрекера',
            'dialogClass'   : 'wp-dialog',
            'width'         : '80%',
            'height'        : $(window).height()-100,
            'modal'         : true,
            //'autoOpen'      : false, 
            'closeOnEscape' : true,      
            'buttons'       : {
              "Закрыть": function() {
                $(this).dialog('close');
              }
            }
          }
        );
      $('.to-rutracker-text').setSelection(0, text.length);
    };
    
    /*
     * Cut string, word safe
     */
    var cutString = function cutString(s, n){
      var cut= s.indexOf(' ', n);
      if(cut== -1) return s;
      return s.substring(0, cut)
    };

    
    
    $.fn.waitUntilExists = function (handler, shouldRunHandlerOnce, isChild) {
      var found       = 'found';
      var $this       = $(this.selector);
      var $elements   = $this.not(function () { return $(this).data(found); }).each(handler).data(found, true);

      if (!isChild) {
          (window.waitUntilExists_Intervals = window.waitUntilExists_Intervals || {})[this.selector] =
              window.setInterval(function () { $this.waitUntilExists(handler, shouldRunHandlerOnce, true); }, 500)
          ;
      } else if (shouldRunHandlerOnce && $elements.length) {
          window.clearInterval(window.waitUntilExists_Intervals[this.selector]);
      }

      return $this;
    }


  function qBBCode(textarea) { this.construct(textarea) }
  qBBCode.prototype = {
	  VK_TAB:		 9,
	  VK_PAGE_UP: 33,
	  BRK_OP:		 '[',
    BRK_CL:     ']',
    textarea:   null,
    stext:      '',
    quoter:     null,
    qouted_pid: null,
    collapseAfterInsert: false,
    replaceOnInsert: false,
    hotKeyClicked: false,

    // Create new BBCode control.
    construct: function(textarea) {
      this.textarea = textarea
      this.tags     = new Object();
      // Tag for quoting.
      this.addTag(
        '_quoter',
        function() { return '[quote="'+th.quoter+'"][qpost='+th.qouted_pid+']' },
        '[/quote]\n',
        null,
        null,
        function() { th.collapseAfterInsert=true; return th._prepareMultiline(th.quoterText) }
      );

      // Init events.
      var th = this;
      //addEvent(textarea, 'keydown',   function(e) { return th.onKeyPress(e, window.HTMLElement? 'down' : 'press') });
      //addEvent(textarea, 'keypress',  function(e) { return th.onKeyPress(e, 'press') });
    },

    // Insert poster name or poster quotes to the text.
    onclickPoster: function(name,post_id) {
      var sel = this.getSelection()[0];
		  if (sel) {
			  this.quoter = name;
			  this.qouted_pid = post_id;
			  this.quoterText = sel;
			  this.insertTag('_quoter');
		  } else {
			  this.insertAtCursor("[b]" + name + '[/b]\n');
		  }
		  return false;
	  },

	  // Quote selected text
	  onclickQuoteSel: function() {
		  var sel = this.getSelection()[0];
		  if (sel) {
			  this.insertAtCursor('[quote]' + sel + '[/quote]\n');
		  }
		  else {
			  alert('Please select text.');
		  }
		  return false;
	  },

	  // Quote selected text
	  emoticon: function(em) {
		  if (em) {
			  this.insertAtCursor(' ' + em + ' ');
		  }
		  else {
			  return false;
		  }
		  return false;
	  },

	  // For stupid Opera - save selection before mouseover the button.
	  refreshSelection: function(get) {
      if (get) this.stext = this.getSelection()[0];
      else this.stext = '';
    },

    // Return current selection and range (if exists).
    // In Opera, this function must be called periodically (on mouse over,
    // for example), because on click stupid Opera breaks up the selection.
    getSelection: function() {
      var w = window;
      var text='', range;
      if (w.getSelection) {
        // Opera & Mozilla?
        text = w.getSelection();
      } else if (w.document.getSelection) {
        // the Navigator 4.0x code
        text = w.document.getSelection();
      } else if (w.document.selection && w.document.selection.createRange) {
        // the Internet Explorer 4.0x code
        range = w.document.selection.createRange();
        text = range.text;
      } else {
        return [null, null];
      }
      if (text == '') text = this.stext;
      text = ""+text;
      text = text.replace("/^\s+|\s+$/g", "");
      return [text, range];
    },

    // Insert string at cursor position of textarea.
    insertAtCursor: function(text) {
      // Focus is placed to textarea.
      var t = this.textarea;
      t.focus();
      // Insert the string.
      if (document.selection && document.selection.createRange) {
        var r = document.selection.createRange();
        if (!this.replaceOnInsert) r.collapse();
        r.text = text;
      } else if (t.setSelectionRange) {
        var start = this.replaceOnInsert? t.selectionStart : t.selectionEnd;
        var end   = t.selectionEnd;
        var sel1  = t.value.substr(0, start);
        var sel2  = t.value.substr(end);
        t.value   = sel1 + text + sel2;
        t.setSelectionRange(start+text.length, start+text.length);
      } else{
        t.value += text;
      }
      // For IE.
      setTimeout(function() { t.focus() }, 100);
    },

    // Surround piece of textarea text with tags.
    surround: function(open, close, fTrans) {
      var t = this.textarea;
      t.focus();
      if (!fTrans) fTrans = function(t) { return t; };

      var rt    = this.getSelection();
      var text  = rt[0];
      var range = rt[1];
      if (text == null) return false;

      var notEmpty = text != null && text != '';

      // Surround.
      if (range) {
        var notEmpty = text != null && text != '';
        var newText = open + fTrans(text) + (close? close : '');
        range.text = newText;
        range.collapse();
        if (text != '') {
          // Correction for stupid IE: \r for moveStart is 0 character.
          var delta = 0;
          for (var i=0; i<newText.length; i++) if (newText.charAt(i)=='\r') delta++;
          range.moveStart("character", -close.length-text.length-open.length+delta);
          range.moveEnd("character", -0);
        } else {
          range.moveEnd("character", -close.length);
        }
        if (!this.collapseAfterInsert) range.select();
      } else if (t.setSelectionRange) {
        var start = t.selectionStart;
        var end   = t.selectionEnd;
        var top   = t.scrollTop;
        var sel1  = t.value.substr(0, start);
        var sel2  = t.value.substr(end);
        var sel   = fTrans(t.value.substr(start, end-start));
        var inner = open + sel + close;
        t.value   = sel1 + inner + sel2;
        if (sel != '') {
          t.setSelectionRange(start, start+inner.length);
          notEmpty = true;
        } else {
          t.setSelectionRange(start+open.length, start+open.length);
          notEmpty = false;
        }
        t.scrollTop = top;
        if (this.collapseAfterInsert) t.setSelectionRange(start+inner.length, start+inner.length);
      } else {
        t.value += open + text + close;
      }
      this.collapseAfterInsert = false;
      return notEmpty;
    },

    // Internal function for cross-browser event cancellation.
    _cancelEvent: function(e) {
      if (e.preventDefault) e.preventDefault();
      if (e.stopPropagation) e.stopPropagation();
      return e.returnValue = false;
    },

    // Available key combinations and these interpretaions for phpBB are
    //     TAB              - Insert TAB char
    //     CTRL-TAB         - Next form field (usual TAB)
    //     ALT-ENTER        - Preview
    //     CTRL-ENTER       - Submit
    // The values of virtual codes of keys passed through event.keyCode are
    // Rumata, http://forum.dklab.ru/about/todo/BistrieKlavishiDlyaOtpravkiForm.html
    onKeyPress: function(e, type) {
      // Try to match all the hot keys.
      var key = String.fromCharCode(e.keyCode? e.keyCode : e.charCode);
      for (var id in this.tags) {
        var tag = this.tags[id];
        // Pressed control key?..
        if (tag.ctrlKey && !e[tag.ctrlKey+"Key"]) continue;
        // Pressed needed key?
        if (!tag.key || key.toUpperCase() != tag.key.toUpperCase()) continue;
        // OK. Insert.
        if (e.type == "keydown") this.insertTag(id);
        // Reset event.
        return this._cancelEvent(e);
      }

      // Ctrl+Tab.
      if (e.keyCode == this.VK_TAB && !e.shiftKey && e.ctrlKey && !e.altKey) {
        this.textarea.form.post.focus();
        return this._cancelEvent(e);
      }

      return true;
    },

    // Adds a BB tag to the list.
    addTag: function(id, open, close, key, ctrlKey, multiline) {
      if (!ctrlKey) ctrlKey = "ctrl";
      var tag = new Object();
      tag.id        = id;
      tag.open      = open;
      tag.close     = close;
      tag.key       = key;
      tag.ctrlKey   = ctrlKey;
      tag.multiline = multiline;
      tag.elt       = document.getElementById('qt_content_'+id);
      
      this.tags[id] = tag;
      // Setup events.
      var elt = tag.elt;
      if (elt) {
        var th = this;
        if (elt.type && elt.type.toUpperCase()=="BUTTON") {
          QaddEvent(elt, 'click', function() { th.insertTag(id); return false; });
        }
        if (elt.tagName && elt.tagName.toUpperCase()=="SELECT") {
          QaddEvent(elt, 'change', function() { th.insertTag(id); return false; });
        }
      } else {
        if (id && id.indexOf('_') != 0) return console.log("addTag('"+id+"'): no such element in the form");
      }
    },

    // Inserts the tag with specified ID.
    insertTag: function(id) {
      // Find tag.
      var tag = this.tags[id];
      if (!tag) return alert("Unknown tag ID: "+id);

      // Open tag is generated by callback?
      var op = tag.open;
      if (typeof(tag.open) == "function") op = tag.open(tag.elt);
      var cl = tag.close!=null? tag.close : "/"+op;

      // Use "[" if needed.
      if (op.charAt(0) != this.BRK_OP) op = this.BRK_OP+op+this.BRK_CL;
      if (cl && cl.charAt(0) != this.BRK_OP) cl = this.BRK_OP+cl+this.BRK_CL;

      this.surround(op, cl, !tag.multiline? null : tag.multiline===true? this._prepareMultiline : tag.multiline);
    },

    _prepareMultiline: function(text) {
      text = text.replace(/\s+$/, '');
      text = text.replace(/^([ \t]*\r?\n)+/, '');
      if (text.indexOf("\n") >= 0) text = "\n" + text + "\n";
      return text;
    }

  }


  // from http://www.dustindiaz.com/rock-solid-addevent/
  function QaddEvent( obj, type, fn ) {
	  if (obj.addEventListener) {
		  obj.addEventListener( type, fn, false );
		  QEventCache.add(obj, type, fn);
	  }
	  else if (obj.attachEvent) {
		  obj["e"+type+fn] = fn;
		  obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
		  obj.attachEvent( "on"+type, obj[type+fn] );
		  QEventCache.add(obj, type, fn);
	  }
	  else {
		  obj["on"+type] = obj["e"+type+fn];
	  }
  }

  var QEventCache = function(){
	  var listEvents = [];
	  return {
		  listEvents : listEvents,
		  add : function(node, sEventName, fHandler){
			  listEvents.push(arguments);
		  },
		  flush : function(){
			  var i, item;
			  for(i = listEvents.length - 1; i >= 0; i = i - 1){
				  item = listEvents[i];
				  if(item[0].removeEventListener){
					  item[0].removeEventListener(item[1], item[2], item[3]);
				  };
				  if(item[1].substring(0, 2) != "on"){
					  item[1] = "on" + item[1];
				  };
				  if(item[0].detachEvent){
					  item[0].detachEvent(item[1], item[2]);
				  };
				  item[0][item[1]] = null;
			  };
		  }
	  };
  }();

  if ( typeof(QTags) !== 'undefined' ) {
    $(document).ready(function($){
      window.qbbcode = new qBBCode(document.getElementById('content'));
      window.ctrl = 'ctrl';
      window.edButtons = []
      $('#qt_content_fullscreen').remove();
      var ef = function(){}; // Dummy fn
      
      
      QTags.addButton('codeI', 'I', ef, 'i' );
      QTags.addButton('codeB', 'B', ef);
      QTags.addButton('codeU', 'U', ef);
      QTags.addButton('codeS', 'S', ef);

      QTags.addButton('codeQuote',  'quote', ef);
      QTags.addButton('codeImg',    'img', function(){
        qbbcode.surround('[align=center][img]', '[/img][/align]');
      });
      QTags.addButton('codeUrl',    'url', ef);
      QTags.addButton('codeAlign',    'align-center', ef);
      QTags.addButton('codeSize',    'размер 18', ef);
      
      QTags.addButton('codeHR',       '-', ef);
      QTags.addButton('codeBR',       '¶', ef);
      QTags.addButton('codeList',     'list', ef);
      QTags.addButton('codeList1',     'list #', ef);
      QTags.addButton('codeOpt',      '1.', ef);
      QTags.addButton('codeSpoiler',  'spoiler', ef);
      QTags.addButton('codeCode',     'code', ef);

      QTags.addButton('codeColorWhite', 'белый', ef);
      QTags.addButton('codeColorRed', 'красный', ef);
      QTags.addButton('codeColorYellow', 'желтый', ef);
      QTags.addButton('codeColorGrey', 'серый', ef);
      
      
      QTags.addButton('more','превью для главной','<!--more-->','','t');
      
      QTags.addButton('rutracker','rutracker', rutracker_button_click);
      /*
       * To rutracker
      
      to_rutracker = $('<a>')
          .addClass('edit-slug button hide-if-no-js')
          .text('Для rutracker');

      $('#edit-slug-box').append(to_rutracker);
      */
      // Pure QTags
      // Tada! )))
      $('#qt_content_more').waitUntilExists(function(){
        qbbcode.addTag('codeB', 'b', null, 'B', ctrl);
        qbbcode.addTag('codeI', 'i', null, 'I', ctrl);
        qbbcode.addTag('codeU', 'u', null, 'U', ctrl);
        qbbcode.addTag('codeS', 's', null, 'S', ctrl);

        qbbcode.addTag('codeQuote', 'quote', null,   'Q', ctrl);
        //qbbcode.addTag('codeImg',   'img',   );});
        //null,   'R', ctrl);
        qbbcode.addTag('codeUrl',   'url=',  '/url', 'Y', ctrl);
        qbbcode.addTag('codeAlign',  'align=center',  '/align', '', ctrl);
        qbbcode.addTag('codeSize',  'size=18',  '/size', 'W', ctrl);
        

        qbbcode.addTag('codeHR',      'hr',     '',    '8', ctrl);
        qbbcode.addTag('codeBR',      'br',     '',    '8', ctrl);
        qbbcode.addTag('codeList',    'list',   null,  'L', ctrl);
        qbbcode.addTag('codeList1',    'list=1',   '/list',  'L', ctrl);
        qbbcode.addTag('codeOpt',     '*',      '',    '0', ctrl);
        qbbcode.addTag('codeSpoiler', 'spoiler=""', '/spoiler', '',  ctrl);
        qbbcode.addTag('codeCode',    'code',    null, 'K', ctrl);

        qbbcode.addTag('codeColorWhite',   'color=white',    '/color', 'W', ctrl);
        qbbcode.addTag('codeColorRed',     'color=red',      '/color', '', ctrl);
        qbbcode.addTag('codeColorYellow',     'color=#ca7a1d',      '/color', '', ctrl);
        qbbcode.addTag('codeColorGrey',    'color=grey',     '/color', '', ctrl);
          
        //$('#qt_content_codeAlign').hide();
        //$(document).on('mousedown', '#qt_content_codeImg', function(e){
        //  $('#qt_content_codeAlign').click();
        //})
      });
    });
  }

});
