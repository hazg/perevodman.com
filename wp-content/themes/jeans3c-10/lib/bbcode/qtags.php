<?php
  global $BBCode; $BBCode = new BBCode();
  //remove_filter( 'the_content', 'wpautop' );    
  //add_filter( 'the_content', 'perevodman_add_br', 1000, 2 );
  add_filter( 'wp_insert_post_data' , 'filter_post_data' , '99', 2 );
  function filter_post_data( $data , $postarr ) {
    #left square bracket	&#91;
    #right square bracket	&#93;
    //$data['post_content'] = preg_replace(
    //  '/[(url|)]/'
    // Last slash in shortcode
    $data['post_content'] = preg_replace(
      '/\[url=(.*?)\/\]/',
      '[url=$1]',
      $data['post_content']
    );


    //print_r($data);
      // Change post title
      return $data;
  }
  
  add_action( 'admin_enqueue_scripts', 'qtags_bbcode_scripts' );
  function qtags_bbcode_scripts() {
	  wp_enqueue_style( 'qtags_bbcode', get_template_directory_uri() . '/lib/bbcode/css/qtags.css' );
    wp_enqueue_script( 'qtags_bbcode', get_template_directory_uri() . '/lib/bbcode/js/qtags.js', array(), THEME_VERSION, true );
    wp_enqueue_script( 'qtags_bbcode_select', get_template_directory_uri() . '/lib/js/rangyinputs-jquery.js', array(), THEME_VERSION, true );
  }

/*function perevodman_add_br($content){
  return wpautop($content) ;
}*/

function autop_do_shortcode($content){
  return do_shortcode($content);
}
 

class BBCode {

	// Plugin initialization
	function BBCode() {
		// This version only supports WP 2.5+ (learn to upgrade please!)
		if ( !function_exists('add_shortcode') ) return;

		// Register the shortcodes
    add_shortcode( 'users_count' , array(&$this, 'shortcode_users_count') );
    add_shortcode( 'posts_count' , array(&$this, 'shortcode_posts_count') );
		add_shortcode( 'b' , array(&$this, 'shortcode_bold') );
		add_shortcode( 'i' , array(&$this, 'shortcode_italics') );
		add_shortcode( 'u' , array(&$this, 'shortcode_underline') );
    add_shortcode( 's' , array(&$this, 'shortcode_stroke') );
		add_shortcode( 'url' , array(&$this, 'shortcode_url') );
		add_shortcode( 'img' , array(&$this, 'shortcode_img') );
		add_shortcode( 'quote' , array(&$this, 'shortcode_quote') );
    add_shortcode( 'qpost' , array(&$this, 'shortcode_qpost') );
		add_shortcode( 'color' , array(&$this, 'shortcode_color') );
		add_shortcode( 'list' , array(&$this, 'shortcode_list') );
    add_shortcode( 'spoiler' , array(&$this, 'shortcode_spoiler') );
    add_shortcode( 'hr' , array(&$this, 'shortcode_hr') );
    add_shortcode( 'br' , array(&$this, 'shortcode_br') );
    add_shortcode( 'align' , array(&$this, 'shortcode_align') );
    add_shortcode( 'code' , array(&$this, 'shortcode_code') );
    add_shortcode( 'size' , array(&$this, 'shortcode_size') );
    add_shortcode( 'rutracker' , array(&$this, 'shortcode_rutracker') );
    add_shortcode( 'blog_age', array(&$this, 'shortcode_blog_age') );

	}


	// Users count shortcode
	function shortcode_users_count( $atts = array(), $content = NULL ) {
    $count = count_users();
  	return autop_do_shortcode( "[color #FFFFFF]".$count["total_users"]."[/color]" );
	}

	// Users count shortcode
	function shortcode_posts_count( $atts = array(), $content = NULL ) {

    $published_posts = wp_count_posts()->publish;
    $val = $published_posts - abs(intval(reset($atts)));

		return autop_do_shortcode("[color=#FFFFFF]".$val.'[/color]' );
	}



	// Bold shortcode
	function shortcode_bold( $atts = array(), $content = NULL ) {
		if ( NULL === $content ) return '';

		return '<strong>' . autop_do_shortcode( $content ) . '</strong>';
	}


	// Italics shortcode
	function shortcode_italics( $atts = array(), $content = NULL ) {
		if ( NULL === $content ) return '';

		return '<em>' . autop_do_shortcode( $content ) . '</em>';
	}


	// Underline shortcode
	function shortcode_underline( $atts = array(), $content = NULL ) {
		if ( NULL === $content ) return '';

		return '<div class="shortcode" style="display:inline;text-decoration:underline">' . autop_do_shortcode( $content ) . '</div>';
	}


	// Url shortcode
	function shortcode_url( $atts = array(), $content = NULL ) {
    
    if ( array_key_exists ('self', $atts) ) { $atts[0] = get_permalink(get_the_ID());   }
    else{                                     $atts = $this->fix_atts($atts);           }

		if ( isset($atts[0]) and !empty($atts[0]) ) {  # [url="http://www.google.com/"]Google[/url]
			$url = $atts[0];
			$text = $content;
		}
		else { # [url]http://www.google.com/[/url]
			$url = $text = $content;
		}

		if ( empty($url) ) return '';
		if ( empty($text) ) $text = $url;

    $adds = ' ';
    if (array_key_exists('title', $atts)){
      $adds = $adds.'title="'.$atts['title'].'"';
    }


    return "<a $adds href=\"".$url.'">' . autop_do_shortcode( $text ) . '</a>';
	}


	// Img shortcode
	function shortcode_img( $atts = array(), $content = NULL ) {
		if ( NULL === $content ) return '';
    $adds = ' ';
    if (is_array($atts) and array_key_exists('alt', $atts)){
      $adds .= 'alt="'.$atts['alt'].'"';
    }
    $atts = $this->fix_atts($atts);
    $style = isset($attr[0])?'':'style="float:'.$atts[0].'"';

 

		return "<img $adds $style src=\"".$content.'" />';
	}


	// Quote shortcode
	function shortcode_quote( $atts = array(), $content = NULL ) {
		if ( NULL === $content ) return '';

		return '<blockquote><p>' . do_shortcode( $content ) . '</p></blockquote>';
	}

	// Qpost shortcode
	function shortcode_qpost( $atts = array(), $content = NULL ) {
		if ( NULL === $content ) return '';
    $atts = $this->fix_atts($atts);
		return "<a href='#comment-$atts[0]' >$atts[0]</a>";
	}

	// Stroke shortcode
	function shortcode_stroke( $atts = array(), $content = NULL ) {
		if ( NULL === $content ) return '';

		return '<div class="shortcode" style="display:inline;text-decoration: line-through;">' . autop_do_shortcode( $content ) . '</div>';
	}
  
  // Color shortcode
	function shortcode_color( $atts = array(), $content = NULL ) {
    $atts = $this->fix_atts($atts);

    
		if ( NULL === $content ) return '';
		return '<div class="shortcode" style="display:inline-block; color:'.$atts[0].';">'. autop_do_shortcode( $content ) . '</div>';
	}

  // List shortcode
	function shortcode_list( $atts = array(), $content = NULL ) {
    
    $atts = $this->fix_atts($atts);
    $tag_name = ($atts[0]==1)?'ol':'ul';
	  
		if ( NULL === $content ) return '';
    //$content = preg_replace('/\[\*\](.*)/', '<li>$1</li>', $content);
		$content = "<$tag_name>". autop_do_shortcode( $content ) . "</$tag_name>";
    return preg_replace('/\[\*\](.*)/', '<li>$1</li>', $content);

  }

  // List item shortcode
  function shortcode_list_item( $atts = array(), $content = NULL ) {
	  
		if ( NULL === $content ) return '';

		return '<li>'. autop_do_shortcode( $content ) . '</li>';
  }

  // Spoiler shortcode
	function shortcode_spoiler( $atts = array(), $content = NULL ) {

    $atts = $this->fix_atts($atts);
		
		if ( NULL === $content ) return '';

	  return replace_spoiler_tag( $atts[0], $content);

	}
  // Hr shortcode
  function shortcode_hr( $atts = array(), $content = NULL ) {
		return '<hr />';
	}

  // Br shortcode
  function shortcode_br( $atts = array(), $content = NULL ) {
		return '<br />';
	}

  // P shortcode
	function shortcode_align( $atts = array(), $content = NULL ) {
		if ( NULL === $content ) return '';
    $atts = $this->fix_atts($atts);

		return '<div style="text-align:'.$atts[0].';">'.autop_do_shortcode( $content ) . '</div>';
	}

  // Code shortcode
	function shortcode_code( $atts = array(), $content = NULL ) {
		if ( NULL === $content ) return '';

		return '<pre><p>' . htmlspecialchars( $content ) . '</p></pre>';
	}

  // Size shortcode
	function shortcode_size( $atts = array(), $content = NULL ) {
		if ( NULL === $content or empty($atts) ) return '';
    $atts = $this->fix_atts($atts);

		return '<div class="shortcode" style="display:inline;font-size:'.$atts[0].'px;">' . autop_do_shortcode( $content ) . '</div>';
	}

  // Rutracker shortcode
	function shortcode_rutracker( $atts = array(), $content = NULL ) {
    if ( NULL === $content or empty($atts) ) return '';
		return autop_do_shortcode( $content );
	}

  // Blog age shortcode
	function shortcode_blog_age( $atts = array(), $content = NULL ) {
    if ( empty($atts) ) return '';
    return perevodman_blog_age(reset($atts));
	}

    

	function fix_atts($atts){
    if ( is_array($atts) and array_key_exists('name', $atts)) {
      $atts[0] = $atts['name'];
      return $atts;
    } elseif (isset($atts[0])){
      # 
      # Hack for: 
      # Array ( [0] => ="test [1] => title" )
      #
      if ( 1 == preg_match( '/^="/', $atts[0] ) and 0 == preg_match( '/"$/', $atts[0] )){
        $res = '';
        for ($i=0; $i<sizeof($atts);$i++){
          $res = $res.' '.(string)$atts[$i];
          if ( 0 != preg_match('/"$/', $a)) break;
        }
        $atts[0] = $res;
      }
      
      #$atts[0] = preg_replace('/=name/', '', $atts[0]);
      $atts[0] = preg_replace('/["\']/', '', $atts[0]);
      $atts[0] = preg_replace('/^[= ]*/', '', $atts[0]);
      
      $atts[0] = urldecode($atts[0]);
    	return $atts;
    }
    else{ return array('');}
	}

}
?>
