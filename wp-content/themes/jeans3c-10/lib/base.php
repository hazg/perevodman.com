<?php

  require_once dirname(__FILE__).'/comments.php';
  require_once dirname(__FILE__).'/widgets_control.php';
  require_once dirname(__FILE__).'/access.php';
  require_once dirname(__FILE__).'/permissions.php';
  require_once dirname(__FILE__).'/bbcode.php';
  require_once dirname(__FILE__).'/mediafiles.php';
  require_once dirname(__FILE__).'/seo.php';
  require_once dirname(__FILE__).'/tuneups.php';
  require_once dirname(__FILE__).'/default_content.php';
  require_once dirname(__FILE__).'/pending_rss.php';
  require_once dirname(__FILE__).'/themed_login.php';
  require_once dirname(__FILE__).'/rank.php';
  require_once dirname(__FILE__).'/textdomain_overwrite.php';
  require_once dirname(__FILE__).'/datetime.php';
  require_once dirname(__FILE__).'/sticky_posts.php';
  
  define('THEME_VERSION', '1.1.2');

  /*
  * Javascript admin variables
  */
  add_action( 'admin_head', 'admin_header_js' );
  function admin_header_js(){ 
    global $current_user;
    $user_is_admin = in_array('administrator', $current_user->roles);
    ?>
    <script type="text/javascript">
      window.perevodman = {
          'user': {
            'is_admin': <?php print $user_is_admin?'true':'false'; ?>
          }
      }
    </script>
  <?php
  }


  /*
   * Is it on dev
   */
  function hazg_is_local() {
    return $_SERVER['SERVER_NAME'] == 'perevodman';
  }

  /*
   * Strpos for array of needles
   */
  function strposa($haystack, $needle, $offset=0) {
    if(!is_array($needle)) $needle = array($needle);
    foreach($needle as $query) {
        if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
    }
    return false;
  }

  /*
   * Log function
   */
  function _el($var) {
    error_log(print_r($var, true))  ;
    file_put_contents('/tmp/perevodoman.txt', $var);
  }

  
  # Admin
  add_action( 'admin_enqueue_scripts', 'perevodman_admin_scripts' );
  function perevodman_admin_scripts() {
    global $current_user;
    
    wp_enqueue_script( 'perevodman_scripts', get_template_directory_uri() . '/lib/js/admin.js', array(), THEME_VERSION, true );
    
    if(!in_array('administrator', $current_user->roles)) {
      wp_enqueue_script( 'perevodman_author_scripts', get_template_directory_uri() . '/lib/js/author.js', array(), THEME_VERSION, true );
    }

    wp_register_style( 'perevodman_admin_styles', get_template_directory_uri() . '/lib/css/admin.css', false, THEME_VERSION );
    wp_enqueue_style( 'perevodman_admin_styles' );

  }

  # Screen
  add_action( 'wp_enqueue_scripts', 'perevodman_scripts', 1000 );
  function perevodman_scripts() {
    #global $current_user;
    wp_enqueue_script( 'perevodman_scripts', get_template_directory_uri() . '/lib/js/screen.js', array(), THEME_VERSION, true );
    #if(!in_array('administrator', $current_user->roles)) {
      #  wp_enqueue_script( 'perevodman_seo_scripts', get_template_directory_uri() . '/lib/js/seo.js', array(), '1.0.0', true );
      #}

    wp_register_style( 'perevodman_styles', get_template_directory_uri() . '/lib/css/screen.css', false, THEME_VERSION );
    wp_enqueue_style( 'perevodman_styles' );
  }

?>
