<?php


  add_action('admin_init', 'hazg_admin_access_init', 0);

  function hazg_admin_access_init() {
    do_restructed_pages();
  }

  function do_restructed_pages() {
    global $pagenow;
    $restructed_admin_pages = get_restructed_pages();
    
    foreach($restructed_admin_pages as $restructed_page) {
      remove_menu_page($restructed_page);
      if($pagenow == $restructed_page) {  die();  }
    }
  }

  function get_restructed_pages() {
    global $current_user, $wp_roles;;

    # Not Admin
    if(!in_array('administrator', $current_user->roles)) {
      add_filter('screen_options_show_screen', '__return_false');
    }

    # Author
    if(in_array('author', $current_user->roles)) {
      
      add_action('create_term','disable_new_tags', 10, 3);

      return array        (
        'profile.php',
        'tools.php',
        'edit-comments.php',
        'upload.php'
  	 	);
    }
    return array();
  }
  
  function disable_new_tags($term_id, $tt_id, $taxonomy) {
    if ($taxonomy == 'post_tag') {
      wp_delete_term($term_id, $taxonomy);
    }
  }

?>
