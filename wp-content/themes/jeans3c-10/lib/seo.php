<?php
add_filter( 'the_content', 'cn_nf_url_parse', 1001);

function cn_nf_url_parse( $content ) {

	$regexp = "<a\s[^>]*href=([\"\']??)([^\"\' >]*?)\\1[^>]*>(.*)<\/a>";
  $content = str_replace ('<noindex>', '', $content);
  $content = str_replace ('</noindex>', '', $content);


	if(preg_match_all("/$regexp/siU", $content, $matches, PREG_SET_ORDER)) {
		if( !empty($matches) ) {
			$srcUrl = get_option('siteurl');
			for ($i=0; $i < count($matches); $i++)
			{
			  
				$tag = $matches[$i][0];
				$tag2 = $matches[$i][0];
        $text = $matches[$i][2];
				$url = $matches[$i][2];
				
				$noFollow = '';

				$pattern = '/target\s*=\s*"\s*_blank\s*"/';
				preg_match($pattern, $tag2, $match, PREG_OFFSET_CAPTURE);
				if( count($match) < 1 )
					$noFollow .= ' target="_blank" ';
					
				$pattern = '/rel\s*=\s*"\s*[n|d]ofollow\s*"/';
				preg_match($pattern, $tag2, $match, PREG_OFFSET_CAPTURE);
				if( count($match) < 1 )
					$noFollow .= ' rel="nofollow" ';
        
        
				if (strpos($url,$srcUrl) === false and strpos($url, 'http') === 0) {
          $tag = preg_replace('/<a\s/', "<a $noFollow", $tag);
				  $content = str_replace($tag2,'<noindex>'.$tag.'</noindex>',$content);
				}
			}
		}
	}
	
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}
?>
