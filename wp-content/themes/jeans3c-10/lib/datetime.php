<?php

function plural_form($n, $forms) {
  return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
}
/**
 * Format an interval to show all existing components.
 * If the interval doesn't have a time component (years, months, etc)
 * That component won't be displayed.
 *
 * @param DateInterval $interval The interval
 *
 * @return string Formatted interval string.
 */
function format_interval(DateInterval $i) {

  $result = "";

  if ($i->y) { $result .= $i->y.' '.plural_form($i->y, array('год','года','лет')).' '; }
  if ($i->m) { $result .= $i->m.' '.plural_form($i->m, array('месяц','месяца','месяцев')).' '; }
  if ($i->d) { $result .= $i->d.' '.plural_form($i->d, array('день','дня','дней')).' '; }
  $color='white';
  if (!$i->d and !$i->m){
    $color='red';
  }
  $result = "<span style='color:$color;'>$result</span>";
  /*if ($i->h) { $result .= $interval->format("%h часов "); }
  if ($i->i) { $result .= $interval->format("%i минут "); }
  if ($i->s) { $result .= $interval->format("%s секунд "); }*/

  return $result;
}

function perevodman_blog_age($age){
  $first_date = new DateTime();
  $second_date = new DateTime($age);

  $difference = $first_date->diff($second_date);

  return format_interval($difference);
}

?>
