<?php get_header(); ?>

<div id="main"><div class="content">
<?php 
if (is_home()){

  if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
  elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
  else { $paged = 1; }

  query_posts(array(
    'paged' => $paged,
    'meta_query' => array(
    'relation' => 'AND',
      array('key' => 'pmsticky',  'compare' => 'EXISTS')
    ))
  );
  $sticky_posts = perevodman_display_posts(); 
  wp_reset_query();
  query_posts(array('post__not_in' => $sticky_posts, 'paged' => $paged));
  perevodman_display_posts(); 
}else{
  perevodman_display_posts(); 
}
?>





<?php
function perevodman_display_posts(){
  $ids = array();
?>

	<?php if (have_posts()) : ?>
	<?php $postnumber = '1' ?>

 	  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
 	  <?php /* If this is a category archive */ if (is_home()) { ?>
		<h2 class="pagetitle"><noindex><a target="_blank" href="http://feeds.feedburner.com/perevodman" rel="nofollow"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/rss.png" alt="RSS подписка" title="RSS подписка" width="30" height="30" /></a> <a target="_blank" href="https://twitter.com/PEREVODMAN" rel="nofollow"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/twitter.png" alt="Follow Me" title="Follow Me" width="30" height="30" /></a> <a target="_blank" href="http://www.facebook.com/perevodman" rel="nofollow"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/facebook.png" alt="Переводман на Facebook" title="Переводман на Facebook" width="30" height="30" /></a> <a target="_blank" href="http://vk.com/perevodomania" rel="nofollow"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/vkontakte.png" alt="Переводман Вконтакте" title="Переводман Вконтакте" width="30" height="30" /></a> <a href="http://perevodman.com/about/email/"><img src="http://perevodman.com/wp-content/themes/jeans3c-10/images/mail.png" alt="Подписка на почтовую рассылку" title="Подписка на почтовую рассылку" width="30" height="30" /></a></noindex></h2>
 	  <?php } ?>

		<?php while (have_posts()) : the_post(); ?>
      <?php
        $ids[] = get_the_ID();
      ?>
			<div class="post" id="post-<?php the_ID(); ?>">
				<div class="title">
				<h1><?php the_category(' '); ?> » <a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					<div class="meta"><span style="color: #ff0000;"><?php the_author() ?></span>&nbsp;&nbsp;&nbsp;<?php edit_post_link('Редактировать', ' | ', ' | '); ?></div>
					<div class="date"><?php the_time('j F') ?>,<br /><?php the_time('Y') ?></div>
				</div>
				<div class="entry">
					<?php if (function_exists('the_excerpt_reloaded')) 
						#the_excerpt_reloaded(145, '<a><img>', 'excerpt', TRUE, '');
						1 == 1;
					else
						the_content('');
					?>
					<?php if($postnumber < '3') { ?>
						<div class="google">
							<!-- Put Your Google Ads Here -->
						</div>
					<?php } $postnumber++; ?>
				</div>
				<div class="postmetadata"><?php the_tags( 'Связанные: ', ', ', ''); ?>&nbsp;<span class="comm-star"> Тема: <?php the_category(', ') ?>&nbsp;</span><span class="comm-star"><?php comments_popup_link('', 'Комменты: 1', 'Комменты: %'); ?></span></div>
			</div>
	    <?php endwhile; ?>
		<?php else : ?>
			<?php # NOT FOUND ?>
	<?php endif; ?>
			<?php if(function_exists('wp_pagenavi')) { ?>
				<div class="pagenavi-bg"><div class="pagenavi-r"><div class="pagenavi-l"><?php wp_pagenavi(); ?></div></div></div>
			<?php } else { ?> 
				<p class="center"><?php previous_posts_link('&laquo; следующие новости') ?>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<?php next_posts_link('предыдущие новости &raquo;') ?></p>
			 <?php } ?>
		
<?php 
  return $ids;
}
?></div></div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
