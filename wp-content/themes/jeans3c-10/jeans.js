jQuery(function($) {
  $('#chatoutput').click(function(e) {
    if (! $(e.target).hasClass('name')) return;
    name = $(e.target).text();
    val = $('#chatbarText').val();
    if (val.length) val += ' ';
    $('#chatbarText').focus().val(val + '[' + name + '], ');
  });

  $('#comment').keydown(function (e) {
    if (e.ctrlKey && e.keyCode == 13) {
      $('#commentform').submit();
    }
  });
});
