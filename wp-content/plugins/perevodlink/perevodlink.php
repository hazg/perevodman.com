<?php
/***

Plugin Name: Perevodlink
Description: Устанавливает связь между публикациями и ее категориями/метками, позволяя на страницах последних автоматически выводить списки «привязок».
Author: Yevhen
Version: 0.3

***/

add_action( 'add_meta_boxes', 'perevodlink_add_custom_box' );

add_action( 'save_post', 'perevodlink_save_postdata' );

function perevodlink_add_custom_box() {
    add_meta_box(
        'perevodlink',
        __( 'Добавить перевод в список', 'perevodlink_textdomain' ),
        'perevodlink_inner_custom_box',
        'post',
        'side',
        'core'
    );

    wp_enqueue_script( 'perevodlink', plugins_url( '/perevodlink.js' , __FILE__ ), array( 'jquery' ) );
}

function perevodlink_inner_custom_box( $post ) {}

function perevodlink_save_postdata( $post_id ) {
    global $wpdb;

    if ( wp_is_post_revision( $post_id ) ||
      ! current_user_can( 'edit_post', $post_id )  || 
      ! $_POST['perevodlink'] ||
      $_POST['post_status'] != 'publish' )
        return;


    remove_action('save_post', 'perevodlink_save_postdata');

    $perevodlink_data = $_POST['perevodlink'];

    $tags = explode(',', $_POST['tax_input']['post_tag']);

    $perevodlink = array();
    foreach ($perevodlink_data as $term_name) {
      $taxonomy = (in_array($term_name, $tags)) ? 'post_tag' : 'category';
      $term = get_term_by('name', $term_name, $taxonomy);
      if ($term) {
        $target_post_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM
          $wpdb->postmeta WHERE meta_key = 'perevodlink' AND meta_value = %s",
          $term->slug
        ) );

        if ( $target_post_id ) {
          include_once dirname( __FILE__ ) . '/simple_html_dom.php';
          
          $target_post = get_post( $target_post_id );
          $custom = get_post_custom($target_post_id);

          $html = $target_post->post_content;
          $title  = stripslashes( $_POST['post_title'] );
          $url    = rtrim(get_permalink( $post_id ), '/');
          
          $html = preg_replace('/\[list(.*)\]/', "[list$1]\n[*][url=$url]$title"."[/url]", $html, 1);
          //put_file_content('/tmp/test.html', $html);

          /*$html->find( 'ol', 0 )->innertext = '<li><a href="' .  get_permalink(
          $post_id ) . '">' . stripslashes($_POST['post_title']) . '</a></li>' . $html->find(
          'ol', 0 )->innertext;*/

          $target_post->post_content = $html;
          if (wp_update_post($target_post)) {
            foreach ( $custom as $key => $values ) {
              delete_post_meta( $target_post_id, $key );
              foreach ( $values as $value ) {
                add_post_meta( $target_post_id, $key, $value );
              }
            }
          }
        }
      }
    }
    add_action('save_post', 'perevodlink_save_postdata');
}
/*
с ним дело такое
этот плагин добавляет поле "Добавить перевод в список"
потом я выбираю нужную мне страничку
потом, там есть произвольное поле на этой страничке
называется тоже perevodlink
я присваиваю этому полю значение
равное id рубрики или метки
и когда потом обновляется новая страничка и в ней отмечаются рубрики/метки, то в поле "добавить перевод в список" появляются их названия
и если эти названия где то внесены в произвольное поле perevodlink
то плагин на этой страничке ищет первый тег <li>
и вписывает после него название добавляемой странички
http://perevodman.com/avo/kyberpunk/881/, в ней есть первый список
вот perevodlink туда добавляет последние странички, которые в нём были отмечены
пополняет список так сказать автоматом ссылки с названием имею в виду
и вот после срабатывания на целевой страничке он делает код вот таким

*/
?>
