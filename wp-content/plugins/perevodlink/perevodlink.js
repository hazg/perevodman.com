jQuery(function($) {
  var pl = $('#perevodlink'),
      inside = $('.inside', pl);

  $('input.tagadd').click(function() {
    val = $(this).siblings('.newtag').val().trim();

    if (getItem(val).length) return;

    if (val.substr(-1) == ',')
        val = val.substr(0, val.length-1);

    newEl = newVal(val);
    inside.append(newEl);
  });

  $('.tagchecklist').click(function(e) {
    if (!$(e.target).hasClass('ntdelbutton')) return;

    val = $(e.target).parent().text().substr(2);

    taxonomyItem = getItem(val);
    delItem(taxonomyItem);
  });

  $('#categorychecklist input').click(function() {
    val = $(this).parent().text().trim();

    if ($(this).is(':checked')) {
      newEl = newVal(val);
      inside.append(newEl);
    } else {
      taxonomyItem = getItem(val);
      delItem(taxonomyItem);
    }
  });

  function newVal(val) {
    input = $('<input>').prop('type', 'checkbox')
      .prop('name', 'perevodlink[]')
      .prop('checked', true)
      .val(val);

    label = $('<label>').addClass('perevodlink')
      .text(val);

    div = $('<div>').append(input).append(label);

    return div;
  }

  function getItem(val) {
    taxonomyItem = $('label', pl).filter(function() {
      if ($(this).text() == val) return $(this);
    });

    return taxonomyItem.parent();
  }
  
  function delItem(item) {
    item.remove();
  }
});
