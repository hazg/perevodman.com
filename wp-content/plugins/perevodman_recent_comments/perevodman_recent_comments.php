<?php
# Plugin Name: Perevodman Recent Comments
# Plugin URI: http://perevodman.com
# Description: Recent comments for perevodman.com
# Author: hazg
# Version: 0.1
# Author URI: http://hazg.net/
class WP_Widget_Perevodman_Recent_Comments extends WP_Widget {

	  function __construct() {
  		$widget_ops = array('classname' => 'widget_perevodman_recent_comments', 'description' => __( 'The most recent comments' ) );
		  parent::__construct('perevodman-recent-comments', __('Последнее прокомментированное'), $widget_ops);
		  $this->alt_option_name = 'widget_perevodman_recent_comments';


		  add_action( 'comment_post', array(&$this, 'flush_widget_cache') );
		  add_action( 'transition_comment_status', array(&$this, 'flush_widget_cache') );
	  }


	  function flush_widget_cache() {
		  wp_cache_delete('widget_perevodman_recent_comments', 'widget');
	  }

	  function widget( $args, $instance ) {
		  global $comments, $comment;

		  $cache = wp_cache_get('widget_perevodman_recent_comments', 'widget');

		  if ( ! is_array( $cache ) )
			$cache = array();

		  if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		  if ( isset( $cache[ $args['widget_id'] ] ) ) {
			  echo $cache[ $args['widget_id'] ];
			  return;
		  }

 		  extract($args, EXTR_SKIP);
 		  $output = '';
		  $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Comments' ) : $instance['title'], $instance, $this->id_base );

		  if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
 			  $number = 5;

		  global $wpdb;

      $results = $wpdb->get_results("SELECT ID, post_title, post_content, post_excerpt, comment_ID, comment_author
          FROM $wpdb->posts 
          JOIN $wpdb->comments ON $wpdb->posts.ID=$wpdb->comments.comment_post_ID 
          WHERE comment_ID IN (SELECT MAX(comment_ID) 
          FROM $wpdb->comments WHERE comment_approved = '1' AND comment_type='' AND comment_post_ID = $wpdb->posts.ID 
          GROUP BY comment_post_ID) AND post_status ='publish' AND post_type='post' AND post_password =''
          ORDER BY comment_ID DESC LIMIT $number");
      $output .= $before_widget;
      
      if ( $title )
  		  $output .= $before_title . $title . $after_title;

      $output .= '<ul id="recentcomments">';

      foreach ($results as $id) {
        $post = &get_post( $id->ID );
        setup_postdata($post);
        $permalink = get_permalink($id->ID);
        $title = get_the_title($id->ID);
        $title = (mb_strlen($title) > 23) ? mb_substr($title, 0, 20).'...' : $title;
  	    $output .=  '<li class="recentcomments">'.
      	$id->comment_author . ' >' .
				    sprintf(
				      _x('%1$s', 'widgets'), 
				      '<a href="' . 
				        "$permalink#comment-".$id->comment_ID.
				        '">' . $title . '</a>') 
				      . '</li>';
	    }
 

 
      $output .= '</ul>';
		  $output .= $after_widget;
		  
		  echo $output;
		  $cache[$args['widget_id']] = $output;
		  wp_cache_set('widget_perevodman_recent_comments', $cache, 'widget');
	  }

	  function update( $new_instance, $old_instance ) {
		  $instance = $old_instance;
		  $instance['title'] = strip_tags($new_instance['title']);
		  $instance['number'] = absint( $new_instance['number'] );
		  $this->flush_widget_cache();

		  $alloptions = wp_cache_get( 'alloptions', 'options' );
		  if ( isset($alloptions['widget_perevodman_recent_comments']) )
			delete_option('widget_perevodman_recent_comments');

		  return $instance;
	  }

	  function form( $instance ) {
		  $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		  $number = isset($instance['number']) ? absint($instance['number']) : 5;
    ?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of comments to show:'); ?></label>
		<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php
	  }
  }

  function wp_perevodman_recent_comments_init() {
  	  if ( !is_blog_installed() )
		  return;

	  register_widget('WP_Widget_Perevodman_Recent_Comments');
  }
  add_action('widgets_init', 'wp_perevodman_recent_comments_init', 1);

?>
